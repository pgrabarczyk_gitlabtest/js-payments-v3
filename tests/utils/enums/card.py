from enum import Enum

from utils.date_util import convert_to_string, adjust_date_day, get_current_time, date_formats
from utils.enums.card_type import CardType


class Card(Enum):
    def __init__(self, formatted_number, expiration_date: str, card_type: CardType, cvv):
        self.formatted_number = formatted_number
        self.__expiration_date = expiration_date
        self.type = card_type.name
        self.cvv = cvv

    # by default expiration date will be future but it may be also fixed value
    # e.g. 11/22
    # or by using more descriptive value
    # like PAST / FUTURE
    # e.g. MASTERCARD_INVALID_EXP_DATE_CARD

    AMEX_CARD = '3400 000000 00611', '', CardType.AMEX, 1234
    AMEX_NON_FRICTIONLESS = '3400 0000 0001 098', '', CardType.AMEX, 1234
    AMERICAN_EXPRESS_FAILED_AUTH_CARD = '3400 0000 0000 033', '', CardType.AMERICANEXPRESS, 1234
    AMERICAN_EXPRESS_TIMEOUT_CARD = '3400 0000 0008 309', '', CardType.AMERICANEXPRESS, 1234
    AMERICAN_EXPRESS_UNAVAILABLE_CARD = '3400 0000 0007 780', '', CardType.AMERICANEXPRESS, 1234

    ASTROPAYCARD_CARD = '1801 0000 0000 0901', '', CardType.ASTROPAYCARD, 123

    JCB_CARD = '3528 0000 0000 0411', '', CardType.JCB, 123

    DINERS_CARD = '3000 000000 000111', '', CardType.DINERS, 123
    DISCOVER_CARD = '6011 0000 0000 0301', '', CardType.DISCOVER, 123
    DISCOVER_BYPASSED_AUTH_CARD = '6011 9900 0000 0006', '', CardType.DISCOVER, 123
    DISCOVER_PASSIVE_AUTH_CARD = '6011 0000 0000 0038', '', CardType.DISCOVER, 123

    MAESTRO_CARD = '5000 0000 0000 0611', '', CardType.MAESTRO, 123
    MASTERCARD_CARD = '5100 0000 0000 0511', '', CardType.MASTERCARD, 123
    MASTERCARD_DECLINED_CARD = '5100 0000 0000 0412', '', CardType.MASTERCARD, 123
    MASTERCARD_INVALID_EXP_DATE_CARD = '5100 0000 0000 0511', '10/18', CardType.MASTERCARD, 123
    MASTERCARD_INVALID_CVV_CARD = '5100 0000 0000 0511', '', CardType.MASTERCARD, 1
    MASTERCARD_SUCCESSFUL_AUTH_CARD = '52000 00000 0000 07', '', CardType.MASTERCARD, 123
    MASTERCARD_FIXED_EXP_DATE_CARD = '52000 00000 0000 07', '09/22', CardType.MASTERCARD, 123
    MASTERCARD_INVALID_PATTERN_CARD = '5100 0000 0000 0510', '13/13', CardType.MASTERCARD, 12
    MASTERCARD_CMPI_AUTH_ERROR_CARD = '5200 0000 0000 0098', '', CardType.MASTERCARD, 123
    MASTERCARD_AUTH_UNAVAILABLE_CARD = '5200 0000 0000 0031', '', CardType.MASTERCARD, 123
    MASTERCARD_PROMPT_FOR_WHITELIST = '5200 0000 0000 2003', '', CardType.MASTERCARD, 123
    MASTERCARD_SUPPORT_TRANS_STATUS_I = '5200 0000 0000 2029', '', CardType.MASTERCARD, 123
    MASTERCARD_NOT_ENROLLED_CARD = '5200 0000 0000 0056', '', CardType.MASTERCARD, 123

    # Cardinal Commerce Test Cards
    MASTERCARD_SUCCESSFUL_FRICTIONLESS_AUTH = '5200 0000 0000 1005', '', CardType.MASTERCARD, 123
    MASTERCARD_FAILED_FRICTIONLESS_AUTH = '5200 0000 0000 1013', '', CardType.MASTERCARD, 123
    MASTERCARD_FRICTIONLESS = '5200 0000 0000 1021', '', CardType.MASTERCARD, 123
    MASTERCARD_UNAVAILABLE_FRICTIONLESS_AUTH = '5200 0000 0000 1039', '', CardType.MASTERCARD, 123
    MASTERCARD_REJECTED_FRICTIONLESS_AUTH = '5200 0000 0000 1047', '', CardType.MASTERCARD, 123
    MASTERCARD_AUTH_NOT_AVAILABLE_ON_LOOKUP = '5200 0000 0000 1054', '', CardType.MASTERCARD, 123
    MASTERCARD_ERROR_ON_LOOKUP = '5200 0000 0000 1062', '', CardType.MASTERCARD, 123
    MASTERCARD_TIMEOUT_ON_CMPI_LOOKUP_TRANSACTION = '5200 0000 0000 1070', '', CardType.MASTERCARD, 123
    MASTERCARD_NON_FRICTIONLESS = '5200 0000 0000 1096', '', CardType.MASTERCARD, 123
    MASTERCARD_STEP_UP_AUTH_FAILED = '5200 0000 0000 1104', '', CardType.MASTERCARD, 123
    MASTERCARD_STEP_UP_AUTH_UNAVAILABLE = '5200 0000 0000 1112', '', CardType.MASTERCARD, 123
    MASTERCARD_ERROR_ON_AUTH = '5200 0000 0000 1120', '', CardType.MASTERCARD, 123
    MASTERCARD_BYPASSED_AUTH = '5200 0000 0000 1088', '', CardType.MASTERCARD, 123

    VISA_CARD = '4111 1100 0000 0211', '', CardType.VISA, 123
    VISA_FAILED_SIGNATURE_CARD = '4000 0000 0000 0010', '', CardType.VISA, 123
    VISA_MERCHANT_NOT_ACTIVE_CARD = '4000 0000 0000 0077', '', CardType.VISA, 123
    VISA_CMPI_LOOKUP_ERROR_CARD = '4000 0000 0000 0085', '', CardType.VISA, 123
    VISA_PRE_WHITELISTED_VISABASE_CONFIG = '4000 0000 0000 2016', '', CardType.VISA, 123
    VISA_DECLINED_CARD = '4242 4242 4242 4242', '', CardType.VISA, 123
    VISA_INVALID_CVV = '4111 1100 0000 0211', '', CardType.VISA, 1235

    VISA_V21_SUCCESSFUL_FRICTIONLESS_AUTH = '4000 0000 0000 1000', '', CardType.VISA, 123
    VISA_V21_FAILED_FRICTIONLESS_AUTH = '4000 0000 0000 1018', '', CardType.VISA, 123
    VISA_V21_FRICTIONLESS = '4000 0000 0000 1026', '', CardType.VISA, 123
    VISA_V21_UNAVAILABLE_FRICTIONLESS_AUTH = '4000 0000 0000 1034', '', CardType.VISA, 123
    VISA_V21_REJECTED_FRICTIONLESS_AUTH = '4000 0000 0000 1042', '', CardType.VISA, 123
    VISA_V21_AUTH_NOT_AVAILABLE_ON_LOOKUP = '4000 0000 0000 1059', '', CardType.VISA, 123
    VISA_V21_ERROR_ON_LOOKUP = '4000 0000 0000 1067', '', CardType.VISA, 123
    VISA_V21_TIMEOUT_ON_CMPI_LOOKUP_TRANSACTION = '4000 0000 0000 1075', '', CardType.VISA, 123
    VISA_V21_NON_FRICTIONLESS = '4000 0000 0000 1091', '', CardType.VISA, 123
    VISA_V21_STEP_UP_AUTH_FAILED = '4000 0000 0000 1109', '', CardType.VISA, 123
    VISA_V21_STEP_UP_AUTH_UNAVAILABLE = '4000 0000 0000 1117', '', CardType.VISA, 123
    VISA_V21_ERROR_ON_AUTH = '4000 0000 0000 1125', '', CardType.VISA, 123
    VISA_V21_BYPASSED_AUTH = '4000 0000 0000 1083', '', CardType.VISA, 123

    VISA_V22_SUCCESSFUL_FRICTIONLESS_AUTH = '4000 0000 0000 2701', '', CardType.VISA, 123
    VISA_V22_FAILED_FRICTIONLESS_AUTH = '4000 0000 0000 2925', '', CardType.VISA, 123
    VISA_V22_FRICTIONLESS = '4000 0000 0000 2719', '', CardType.VISA, 123
    VISA_V22_UNAVAILABLE_FRICTIONLESS_AUTH = '4000 0000 0000 2313', '', CardType.VISA, 123
    VISA_V22_REJECTED_FRICTIONLESS_AUTH = '4000 0000 0000 2537', '', CardType.VISA, 123
    VISA_V22_AUTH_NOT_AVAILABLE_ON_LOOKUP = '4000 0000 0000 2990', '', CardType.VISA, 123
    VISA_V22_ERROR_ON_LOOKUP = '4000 0000 0000 2446', '', CardType.VISA, 123
    VISA_V22_TIMEOUT_ON_CMPI_LOOKUP_TRANSACTION = '4000 0000 0000 2354', '', CardType.VISA, 123
    VISA_V22_NON_FRICTIONLESS = '4000 0000 0000 2503', '', CardType.VISA, 123
    VISA_V22_STEP_UP_AUTH_FAILED = '4000 0000 0000 2370', '', CardType.VISA, 123
    VISA_V22_STEP_UP_AUTH_UNAVAILABLE = '4000 0000 0000 2420', '', CardType.VISA, 123
    VISA_V22_ERROR_ON_AUTH = '4000 0000 0000 2644', '', CardType.VISA, 123
    VISA_V22_BYPASSED_AUTH = '4000 0000 0000 2560', '', CardType.VISA, 123

    @property
    def number(self):
        # the same number as the formatted one but with 'normal' notation
        return int(self.formatted_number.replace(' ', ''))

    @property
    def value(self) -> str:
        return self.name

    @property
    def expiration_date(self) -> str:
        expiration_date: str = self.__expiration_date
        if not expiration_date or expiration_date.__eq__('FUTURE'):
            return self.future_expiration_date
        elif self.__expiration_date.__eq__('PAST'):
            return self.past_expiration_date
        return expiration_date

    @property
    def future_expiration_date(self) -> str:
        date_two_years_in_future = adjust_date_day(get_current_time(), 2 * 365)
        return str(convert_to_string(date_two_years_in_future, date_formats.month_year))

    @property
    def past_expiration_date(self) -> str:
        date_two_years_in_past = adjust_date_day(get_current_time(), -2 * 365)
        return str(convert_to_string(date_two_years_in_past, date_formats.month_year))
