"""This module consist Behave hooks which allows us to better manage the code workflow"""
# -*- coding: utf-8 -*-

# -- FILE: features/environment.py
# USE: behave -D BEHAVE_DEBUG_ON_ERROR         (to enable  debug-on-error)
# USE: behave -D BEHAVE_DEBUG_ON_ERROR=yes     (to enable  debug-on-error)
# USE: behave -D BEHAVE_DEBUG_ON_ERROR=no      (to disable debug-on-error)
from logging import INFO

from configuration import CONFIGURATION
from logger import get_logger
from page_factory import PageFactory
from utils.browser import Browser
from utils.driver_factory import DriverFactory
from utils.extensions import WebElementsExtensions
from utils.helpers.request_executor import mark_test_as_failed, set_scenario_name, mark_test_as_passed
from utils.mock_handler import MockServer
from utils.reporter import Reporter
from utils.test_data import TestData
from utils.visual_regression.screenshot_manager import ScreenshotManager
from utils.waits import Waits

BEHAVE_DEBUG_ON_ERROR = False
LOGGER = get_logger(INFO)


def setup_debug_on_error(userdata):
    # pylint: disable=global-statement

    """Debug-on-Error(in case of step failures) providing, by using after_step() hook.
    The debugger starts when step definition fails"""
    global BEHAVE_DEBUG_ON_ERROR
    BEHAVE_DEBUG_ON_ERROR = userdata.getbool('BEHAVE_DEBUG_ON_ERROR')


def before_all(context):
    """Run before the whole shooting match"""
    context.configuration = CONFIGURATION
    MockServer.start_mock_server()


def disable_headless_for_visa_checkout(context):
    if 'visa_checkout' in context.scenario.tags:
        context.configuration.HEADLESS = False


def before_scenario(context, scenario):
    """Run before each scenario"""
    LOGGER.info('BEFORE SCENARIO')
    if context.configuration.REMOTE:
        context.configuration.BROWSER = context.configuration.REMOTE_BROWSER
    disable_headless_for_visa_checkout(context)
    context.browser = context.configuration.BROWSER
    context.driver_factory = DriverFactory(configuration=context.configuration)
    context.waits = Waits(driver_factory=context.driver_factory, configuration=context.configuration)
    extensions = WebElementsExtensions(driver_factory=context.driver_factory, configuration=context.configuration)
    context.executor = Browser(driver_factory=context.driver_factory, configuration=context.configuration)
    context.reporter = Reporter(driver_factory=context.driver_factory, configuration=context.configuration)
    context.screenshot_manager = ScreenshotManager(driver_factory=context.driver_factory, configuration=context.configuration)
    context.page_factory = PageFactory(executor=context.executor, extensions=extensions,
                                       reporter=context.reporter, configuration=context.configuration,
                                       wait=context.waits)
    context.test_data = TestData(configuration=context.configuration)
    context.session_id = context.executor.get_session_id()
    context.language = 'en_GB'
    scenario.name = '%s executed on %s' % (scenario.name, context.browser.upper())
    LOGGER.info(scenario.name)
    validate_if_proper_browser_is_set_for_test(context, scenario)


def after_scenario(context, scenario):
    """Run after each scenario"""
    LOGGER.info('AFTER SCENARIO')
    if scenario.status == 'failed':
        LOGGER.info('Printing console logs:')
        for entry in context.driver_factory.get_driver().get_log('browser'):
            LOGGER.info(entry)
    browser_name = context.browser
    context.executor.clear_cookies()
    context.executor.close_browser()
    MockServer.stop_mock_server()
    if context.configuration.REMOTE:
        set_scenario_name(context.session_id, scenario.name)
    scenario.name = f'{scenario.name}_{browser_name.upper()}'
    if scenario.status == 'failed' and context.configuration.REMOTE:
        mark_test_as_failed(context.session_id)
    elif context.configuration.REMOTE:
        mark_test_as_passed(context.session_id)


def after_step(context, step):
    """Run after each step"""
    if step.status == 'failed':
        scenario_name = _clean(context.scenario.name.title())
        feature_name = _clean(context.feature.name.title())
        step_name = _clean(step.name.title())
        filename = f'{feature_name}_{scenario_name}_{step_name}'
        context.reporter.save_screenshot_and_page_source(filename)


def _clean(text_to_clean):
    """Method to clean text which will be used for tests run reporting"""
    text = ''.join(x if x.isalnum() else '' for x in text_to_clean)
    return text


def validate_if_proper_browser_is_set_for_test(context, scenario):
    if 'apple_test' in scenario.tags and (context.browser.upper() not in 'SAFARI'):
        if 'iP' not in CONFIGURATION.REMOTE_DEVICE:
            scenario.skip('SCENARIO SKIPPED as iOS system and Safari is required for ApplePay test')
    if 'visa_test' in scenario.tags and ('IE' in CONFIGURATION.REMOTE_BROWSER):
        scenario.skip('SCENARIO SKIPPED as IE browser doesn\'t support Visa Checkout')
    if 'animated_card_repo_test' in scenario.tags:
        context.is_field_in_iframe = False
        # ToDo Temporarily disabled parent-iframe test. Problem with cress-origin restriction on ios
    if 'parent_iframe' in scenario.tags and ('iP' in CONFIGURATION.REMOTE_DEVICE):
        scenario.skip('Temporarily disabled test ')
    if 'ignore_on_headless' in scenario.tags and not context.configuration.REMOTE:
        scenario.skip('Scenario skipped for headless chrome')
    else:
        context.is_field_in_iframe = True
