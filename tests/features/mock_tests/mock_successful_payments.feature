Feature: Successfull payments with various configurations

  As a user
  I want to use card payments method
  In order to check full payment functionality

  Background:
    Given JavaScript configuration is set for scenario based on scenario's @config tag

  @base_config @extended_tests_part_1 @cardinal_commerce
  Scenario Outline: Successful payment using most popular Credit Cards: <card_type>
    Given User opens page with payment form
    When User fills payment form with credit card number "<card_number>", expiration date "<expiration_date>" and cvv "<cvv>"
    And Frictionless THREEDQUERY, AUTH response is set to OK
    And User clicks Pay button
    Then User will see payment status information: "Payment has been successfully processed"
    And Frictionless AUTH and THREEDQUERY requests were sent only once with correct data

    @smoke_test
    Examples:
      | card_number      | expiration_date | cvv | card_type |
      | 4111110000000211 | 12/22           | 123 | VISA      |

    Examples:
      | card_number      | expiration_date | cvv  | card_type  |
      | 5100000000000511 | 12/22           | 123  | MASTERCARD |
      | 340000000000611  | 12/22           | 1234 | AMEX       |

  @config_update_jwt_true @smoke_test @extended_tests_part_2
  Scenario: Successful payment with updated JWT
    Given User opens prepared payment form page WITH_UPDATE_JWT
      | jwtName          |
      | BASE_UPDATED_JWT |
    When User fills payment form with defined card VISA_V21_NON_FRICTIONLESS
    And User calls updateJWT function by filling amount field
    And THREEDQUERY mock response is set to "ENROLLED_Y"
    And ACS mock response is set to "OK"
    And User clicks Pay button - AUTH response is set to "OK"
    Then User will see payment status information: "Payment has been successfully processed"
    And User will see that notification frame has "green" color
    And AUTH and THREEDQUERY requests were sent only once with correct data
    And JSINIT requests contains updated jwt

  @config_defer_init
  Scenario: Successful payment with deferInit
    Given User opens page with payment form
    When User fills payment form with defined card VISA_V21_NON_FRICTIONLESS
    And THREEDQUERY mock response is set to "ENROLLED_Y"
    And ACS mock response is set to "OK"
    And User clicks Pay button - AUTH response is set to "OK"
    Then User will see payment status information: "Payment has been successfully processed"
    Then JSINIT request was sent only once
    And AUTH and THREEDQUERY requests were sent only once with correct data

  @config_defer_init
  Scenario: Successful payment with deferInit and updated JWT
    Given User opens prepared payment form page WITH_UPDATE_JWT
      | jwtName          |
      | BASE_UPDATED_JWT |
    When User fills payment form with defined card VISA_V21_NON_FRICTIONLESS
    And THREEDQUERY mock response is set to "ENROLLED_Y"
    And ACS mock response is set to "OK"
    And User calls updateJWT function by filling amount field
    And User clicks Pay button - AUTH response is set to "OK"
    Then User will see payment status information: "Payment has been successfully processed"
    And User will see that notification frame has "green" color
    And AUTH and THREEDQUERY requests were sent only once with correct data
    And JSINIT requests contains updated jwt

  @config_submit_cvv_only @extended_tests_part_2
  @submit_cvv_only
  Scenario: Successful payment when cvv field is selected to submit
    Given User opens page with payment form
    When User fills "SECURITY_CODE" field "123"
    And THREEDQUERY, AUTH mock response is set to OK
    And User clicks Pay button
    Then User will see payment status information: "Payment has been successfully processed"
    And User will not see CARD_NUMBER
    And User will not see EXPIRATION_DATE
    And THREEDQUERY, AUTH ware sent only once in one request

  @config_submit_cvv_for_amex
  @submit_cvv_only
  Scenario: Successful payment by AMEX when cvv field is selected to submit
    Given User opens page with payment form
    When User fills "SECURITY_CODE" field "1234"
    And THREEDQUERY, AUTH mock response is set to OK
    And User clicks Pay button
    Then User will see payment status information: "Payment has been successfully processed"
    And User will not see CARD_NUMBER
    And User will not see EXPIRATION_DATE
    And THREEDQUERY, AUTH ware sent only once in one request

  @config_cvvToSubmit_and_submitOnSuccess
  @submit_cvv_only
  Scenario: Successful payment with fieldToSubmit and submitOnSuccess
    Given User opens page with payment form
    When User fills "SECURITY_CODE" field "123"
    And THREEDQUERY mock response is set to "ENROLLED_Y"
    And ACS mock response is set to "OK"
    And User clicks Pay button - AUTH response is set to "OK"
    Then User will be sent to page with url "www.example.com" having params
      | key           | value                                   |
      | errormessage  | Payment has been successfully processed |
      | baseamount    | 1000                                    |
      | currencyiso3a | GBP                                     |
      | errorcode     | 0                                       |
    And THREEDQUERY, AUTH ware sent only once in one request

  @config_skip_jsinit @cardinal_commerce
  Scenario: Successful payment with skipped JSINIT process
    Given User opens page with payment form
    When User fills payment form with defined card VISA_V21_NON_FRICTIONLESS
    And THREEDQUERY mock response is set to "ENROLLED_Y"
    And ACS mock response is set to "OK"
    And User clicks Pay button - AUTH response is set to "OK"
    Then User will see payment status information: "Payment has been successfully processed"
    And User will see that notification frame has "green" color
    And AUTH and THREEDQUERY requests were sent only once with correct data

  @base_config @stopSubmitFormOnEnter
  Scenario: Submit payment form by 'Enter' button
    Given User opens page with payment form
    When User fills payment form with defined card VISA_V21_NON_FRICTIONLESS
    And THREEDQUERY mock response is set to "ENROLLED_Y"
    And ACS mock response is set to "OK"
    And AUTH response is set to "OK"
    And User press ENTER button in input field
    Then User will see payment status information: "Payment has been successfully processed"
    And User will see the same provided data in inputs fields
