import 'location-origin';
import 'whatwg-fetch';
import 'url-polyfill';
import 'reflect-metadata';
import 'core-js/es6/array';
import 'core-js/es6/object';
import 'fastestsmallesttextencoderdecoder';
import '@sheerun/mutationobserver-shim';
