export type RequestType =
  | 'WALLETVERIFY'
  | 'JSINIT'
  | 'THREEDQUERY'
  | 'CACHETOKENISE'
  | 'AUTH'
  | 'ERROR'
  | 'RISKDEC'
  | 'SUBSCRIPTION'
  | 'ACCOUNTCHECK';
