export interface IThreeDSTokens {
  jwt: string;
  cacheToken: string;
}
