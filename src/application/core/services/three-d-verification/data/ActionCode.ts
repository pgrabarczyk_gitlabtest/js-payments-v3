export enum ActionCode {
  SUCCESS = 'SUCCESS',
  NOACTION = 'NOACTION',
  FAILURE = 'FAILURE',
  ERROR = 'ERROR'
}
