import { environment } from '../../../../environments/environment';

export const CARD_NUMBER_COMPONENT_NAME: string = 'cardNumber';
export const CARD_NUMBER_IFRAME: string = 'st-card-number-iframe';
export const CARD_NUMBER_INPUT: string = 'st-card-number-input';
export const CARD_NUMBER_INPUT_SELECTOR: string = 'st-card-number';
export const CARD_NUMBER_MESSAGE: string = 'st-card-number-message';
export const CARD_NUMBER_LABEL: string = 'st-card-number-label';
export const CARD_NUMBER_WRAPPER: string = 'st-card-number__wrapper';

export const EXPIRATION_DATE_COMPONENT_NAME: string = 'expirationDate';
export const EXPIRATION_DATE_IFRAME: string = 'st-expiration-date-iframe';
export const EXPIRATION_DATE_INPUT: string = 'st-expiration-date-input';
export const EXPIRATION_DATE_INPUT_SELECTOR: string = 'st-expiration-date';
export const EXPIRATION_DATE_MESSAGE: string = 'st-expiration-date-message';
export const EXPIRATION_DATE_LABEL: string = 'st-expiration-date-label';
export const EXPIRATION_DATE_WRAPPER: string = 'st-expiration-date__wrapper';

export const SECURITY_CODE_COMPONENT_NAME: string = 'securityCode';
export const SECURITY_CODE_IFRAME: string = 'st-security-code-iframe';
export const SECURITY_CODE_INPUT: string = 'st-security-code-input';
export const SECURITY_CODE_INPUT_SELECTOR: string = 'st-security-code';
export const SECURITY_CODE_MESSAGE: string = 'st-security-code-message';
export const SECURITY_CODE_LABEL: string = 'st-security-code-label';
export const SECURITY_CODE_WRAPPER: string = 'st-security-code__wrapper';

export const NOTIFICATION_FRAME_ID: string = 'st-notification-frame';
export const NOTIFICATION_FRAME_CORE_CLASS: string = 'notification-frame';
export const NOTIFICATION_FRAME_ERROR_CLASS: string = 'notification-frame--error';
export const NOTIFICATION_FRAME_INFO_CLASS: string = 'notification-frame--info';
export const NOTIFICATION_FRAME_SUCCESS_CLASS: string = 'notification-frame--success';
export const NOTIFICATION_FRAME_CANCEL_CLASS: string = 'notification-frame--cancel';

export const CONTROL_FRAME_COMPONENT_NAME: string = 'controlFrame';
export const CONTROL_FRAME_IFRAME: string = 'st-control-frame-iframe';

export const MERCHANT_FORM_SELECTOR: string = 'st-form';
export const MERCHANT_PARENT_FRAME: string = 'st-parent-frame';

export const CARD_NUMBER_COMPONENT: string = `${environment.FRAME_URL}/card-number.html`;
export const EXPIRATION_DATE_COMPONENT: string = `${environment.FRAME_URL}/expiration-date.html`;
export const SECURITY_CODE_COMPONENT: string = `${environment.FRAME_URL}/security-code.html`;
export const CONTROL_FRAME_COMPONENT: string = `${environment.FRAME_URL}/control-frame.html`;
export const ANIMATED_CARD_COMPONENT: string = `${environment.FRAME_URL}/animated-card.html`;

export const ANIMATED_CARD_INPUT_SELECTOR: string = 'st-animated-card';
export const ANIMATED_CARD_COMPONENT_IFRAME: string = 'st-animated-card-iframe';
export const ANIMATED_CARD_COMPONENT_NAME: string = 'animatedCard';
