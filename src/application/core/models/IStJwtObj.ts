export interface IStJwtObj<T = any> {
  payload: T;
}
