export const environment = {
  production: false,
  libraryUrl: 'https://webservices.securetrading.net/js/v2/st.js',
  configUrl: '/assets/config.json'
};
