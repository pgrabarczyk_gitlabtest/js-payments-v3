export default {
  st_library_url: 'https://webservices.securetrading.net:8443/st.js',
  config_url: 'https://webservices.securetrading.net:8443/config.json'
};
