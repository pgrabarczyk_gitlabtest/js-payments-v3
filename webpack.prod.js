const { merge } = require('webpack-merge');
const common = require('./webpack.common.js');
const webpack = require('webpack');
const path = require('path');

module.exports = merge(common, {
  mode: 'production',
  plugins: [
    new webpack.DefinePlugin({
      WEBSERVICES_URL: JSON.stringify('https://webservices.securetrading.net/js/v2'),
      FRAME_URL: JSON.stringify(process.env.npm_config_frame_url),
    })
  ],
  resolve: {
    alias: {
      [path.resolve(__dirname, "src/environments/environment")]:
        path.resolve(__dirname, "src/environments/environment.prod.ts")
    }
  }
});
